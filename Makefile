.PHONY: flake8 clean sdist tests

SHELL := /bin/bash

docker-decoder: sdist
	@$(eval version:=$(shell python3 -c "about={}; exec(open('./decoder/__about__.py').read(), about); print(about['__version__'])"))
	@docker build \
		--build-arg VERSION=$(version) \
		-f docker/Dockerfile.decoder \
		-t ${USER}/decoder .

flake8:
	@git diff -U0 origin/master  -- `find . -name '*.py'` > master-python.diff
	@cat master-python.diff | docker run -i -v ${PWD}:/apps alpine/flake8  --diff > diff-flake8.txt
	@if [ -s diff-flake8.txt ]; then cat diff-flake8.txt; false; fi

clean:
	@find . -type f -name '*.py[co]' -delete -o -type d -name __pycache__ -delete
	@rm -rf ./dist

sdist: clean
	@sed -i s/"%GIT_COMMIT%"/"$${CI_COMMIT_SHA:-${shell git rev-parse HEAD}}"/ ./decoder/__about__.py
	python setup.py sdist
	@git checkout ./decoder/__about__.py

tests:
	@pytest -s -v tests/test_algorithms.py

start-local:
	decoder -c ./decoder/resources/config.json run

start-local-docker: docker-decoder
	docker-compose -f docker/docker-compose-local.yml up
