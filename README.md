# Requirements

 * Python3
 * docker
 * docker-compose
 * make

# General description

Backend API implemented in Python3 based on Flask framework. Backend exposes two endpoints

- healtcheck
- /decode

Endpoints are described in swagger definition file located under *decoder/resources*.
Application is using simple configuration file, which may be passed as a parameter.
Configuration can be overwritten by providing path to configuration file in CONF_FILE env variable.
Due to potentially astronomical number of possible decodings, application enforces limit 
on maximum number of possible decodings returned in a single request to 1000. Response includes information
about offset, start index, last index etc. 

# Instructions

Dockerized application is using *CONF_FILE* environmental variable to get the location of config.
In order to build docker image execute below command

`
$ make docker-decoder
`

In order to start the application locally

`
$ make start-local
`

In order to start the application as docker container with exposed *8080* port for communication

`
$ make start-local-docker
`

it will prepare sdist and build fresh docker image.

Example healthcheck
`
$ curl http://127.0.0.1:8080/healthcheck
`

Example GET request for string '123121212' with offset set to 10 and limit set to 10.

`
$ curl http://127.0.0.1:8080/decode?encoded_string=123121212&limit=10&offset=10
{
  "count": 10,
  "decodings": [
    "ABCLAUB",
    "ABCLLAB",
    "ABCLLL",
    "AWABABAB",
    "AWABABL",
    "AWABAUB",
    "AWABLAB",
    "AWABLL",
    "AWAUBAB",
    "AWAUBL"
  ],
  "encoded": "123121212",
  "first": 10,
  "last": 20,
  "offset": 10,
  "total_count": 34
}
`

There are unit tests implemented for algorithms. In order to execute tests 

`
make tests
`
