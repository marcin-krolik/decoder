from __future__ import absolute_import, division, print_function

from datetime import date

__all__ = [
    "__title__", "__uri__", "__version__", "__author__",
    "__email__", "__copyright__",
]

__title__ = "String decoder"
__uri__ = "https://gitlab.com/marcin-krolik/decoder"

__version__ = "1.0"
__git_commit__ = '%GIT_COMMIT%'

__author__ = "Marcin Krolik"
__email__ = "marcin.krolik@gmail.com"

__copyright__ = "{} Marcin Krolik.".format(date.today().year)
