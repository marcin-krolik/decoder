# Decoding problem grows very quickly, below I calculated number of possible
# decodings of a N-length string of '1's. With length of 40 it explodes to over 60M
# 5=8
# 6=13
# 7=21
# 8=34
# 9=55
# 10=55
# 12=144
# 14=377
# 16=987
# 18=2584
# 20=6765
# 30=317811
# 40=63245986
# 80=5527939700884757

decoding_map = dict(
    zip(range(1, 27), [chr(i).upper() for i in range(97, 123)])
)


def decode_string(encoded):
    """
    Recursive approach to solve decoding problem.
    """
    # if string is a empty string, it can be decoded only as empty string
    if encoded == '':
        yield ''
        raise StopIteration
    # if string is a number [1-9] it can be decoded
    # if string is 0 - it can be decoded, returning 0
    if len(encoded) < 2:
        yield decoding_map.get(int(encoded), encoded)
        raise StopIteration

    def _decode(encoded_, decoded):
        # reaching the leaf of the branch, finish recursion
        if len(encoded_) < 1:
            yield decoded
        # calculate single decoding and move one step deeper
        if len(encoded_) >= 1:
            yield from _decode(encoded_[1:], decoded + decoding_map.get(int(encoded_[0]), ''))
        # calculate merged decoding and move two steps deeper
        if len(encoded_) >= 2:
            # if neighbouring characters can't be decoded merged - noop
            try:
                yield from _decode(encoded_[2:], decoded + decoding_map[int(encoded_[0:2])])
            except KeyError:
                pass

    yield from _decode(encoded, '')


def count_decodings(encoded):
    """
    Helper function to count possible decodings
    """
    # for empty string, there can be only one decoding
    if not len(encoded):
        return 1

    # if string starts with '0' try decoding from next character
    if encoded[0] == '0':
        return count_decodings(encoded[1:])

    preprevious = previous = 1
    for i in range(1, len(encoded)):
        decodings = 0
        if encoded[i] != '0':
            decodings += previous
        else:
            continue
        # if neighbouring characters can't be decoded merged - noop
        try:
            decoding_map[int(encoded[i-1:i+1])]
            # in case previous character was '0', no merge count should happen
            if 10 < int(encoded[i-1:i+1]) < 27:
                decodings += preprevious
        except KeyError:
            continue
        preprevious, previous = previous, decodings

    return previous

