import os

import connexion
from flask_log_request_id import RequestID
from flask.config import Config

_current_path = os.path.dirname(__file__)


def create_app(ctx_cfg):
    """
    Creates the flask app
    """
    cfg = Config('.')

    if ctx_cfg is not None:
        cfg.from_json(ctx_cfg)

    # override context configuration from environment file
    file_cfg = os.environ.get('CONF_FILE')
    if file_cfg is not None and os.path.isfile(file_cfg):
        cfg.from_json(file_cfg)

    connexion_app = connexion.App(
        __name__,
        options={
            "swagger_ui": cfg.get('SWAGGER_UI', False)
        })

    application = connexion_app.app
    RequestID(application)
    application.config.from_mapping(cfg)

    connexion_app.add_api(
        os.path.join(_current_path, 'resources', 'swagger.yml'),
        validate_responses=True,
        strict_validation=True
    )

    return connexion_app
