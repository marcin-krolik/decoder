import click

from decoder.app import create_app


@click.group()
@click.option('--config', '-c', type=click.Path(exists=True, dir_okay=False))
@click.pass_context
def cli(ctx, config=None):
    ctx.ensure_object(dict)
    ctx.obj['CONFIG'] = config if config else None


@cli.command()
@click.pass_context
def run(ctx):
    """
    Starts the app
    :param ctx: cli context
    """
    app = create_app(ctx_cfg=ctx.obj['CONFIG'])
    app.run(threaded=False, port=8080, debug=app.app.config.get('DEBUG', False))
