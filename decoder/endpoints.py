from flask import abort

from decoder.__about__ import __git_commit__
from decoder.algorithms import decode_string, count_decodings


def health_check():
    return {'git_commit': __git_commit__}, 200


def decode(encoded_string, offset, limit):
    offset = int(offset)
    limit = int(limit)

    # response with bad request for incorrect string
    try:
        count = count_decodings(encoded_string)
    except ValueError:
        abort(400, "Can't decode string")

    # also bad request when offset is bigger than count
    if offset >= count:
        abort(400, 'Offset {} >= {} decodings'.format(offset, count))

    length = min(count - offset, limit)

    resp = {"decodings": [],
            "encoded": encoded_string,
            "count": length,
            "total_count": count,
            "offset": offset,
            "first": offset,
            "last": offset + length
            }

    # generate response based on provided limit and offset
    while len(resp['decodings']) < length:
        for i, decoding in enumerate(decode_string(encoded_string)):
            if i > offset + length:
                break
            if offset <= i < offset + length:
                resp["decodings"].append(decoding)
    return resp
