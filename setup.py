import os
from setuptools import setup, find_packages

ROOT = os.path.realpath(os.path.join(os.path.dirname(__file__)))

about = {}
with open(os.path.join(ROOT, 'decoder', '__about__.py')) as f:
    exec(f.read(), about)  # nosec: about file is benign

REQUIREMENTS = []
rfiles = ['requirements.txt']

for rfile in rfiles:
    with open(os.path.join(os.path.dirname(__file__), rfile)) as f:
        reqs = [l.strip() for l in f.readlines()
                if not l.strip().startswith(('#', '-r'))]
        REQUIREMENTS.extend(reqs)

setup(
    name='decoder',
    version=about['__version__'],
    url=about['__uri__'],
    author=about['__author__'],
    author_email=['__mail__'],
    packages=find_packages(exclude=['tests']),
    include_package_data=True,
    data_files=rfiles,
    zip_safe=False,
    entry_points={
        'console_scripts': [
            'decoder=decoder.commands:cli',
        ],
    },
    install_requires=REQUIREMENTS,
    classifiers=[
        'Framework :: Flask',
        'Operating System :: OS Independent',
        'Topic :: Software Development',
        "Programming Language :: Python :: 3.7",
        "Natural Language :: English",
        "License :: OSI Approved :: Apache Software License"
    ]
)
