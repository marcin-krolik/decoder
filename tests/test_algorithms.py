import pytest

from decoder.algorithms import decode_string, count_decodings


@pytest.mark.parametrize("encoded_string,expected_decodings_count",[
    ('', 1),
    ('1', 1),
    ('12', 2),
    ('226', 3),
    ('111', 3),
    ('333', 1),
    ('3313', 2),
    ('123456789',3),
    ('23456789',2),
    ('3456789',1),
    ('11111', 8),
    ('111111', 13),
    ('1111111', 21),
])
def test_count_decodings(encoded_string, expected_decodings_count):
    count = count_decodings(encoded_string)
    assert count == expected_decodings_count, 'Has {}, expected {}'.format(count, expected_decodings_count)



@pytest.mark.parametrize("encoded_string,expected_decodings",[
    ('', ['']),
    ('1', ['A']),
    ('12', ['AB', 'L']),
    ('226', ['BBF', 'BZ', 'VF']),
    ('111', ['AAA', 'AK', 'KA']),
    ('333', ['CCC']),
    ('3313', ['CCAC', 'CCM']),
    ('123456789', ['ABCDEFGHI', 'AWDEFGHI', 'LCDEFGHI']),
    ('23456789', ['BCDEFGHI', 'WDEFGHI']),
    ('3456789', ['CDEFGHI']),
    ('11111', ['AAAAA', 'AAAK', 'AAKA', 'AKAA', 'AKK', 'KAAA', 'KAK', 'KKA']),
    ('111111', ['AAAAAA', 'AAAAK', 'AAAKA', 'AAKAA', 'AAKK', 'AKAAA', 'AKAK', 'AKKA', 'KAAAA', 'KAAK', 'KAKA', 'KKAA', 'KKK']),
])
def test_decodings(encoded_string, expected_decodings):
    decodings = list(decode_string(encoded_string))
    count = count_decodings(encoded_string)
    assert len(decodings) == count, 'Has {}, expected {}'.format(len(decodings), count)
    decodings.sort()
    expected_decodings.sort()
    assert decodings == expected_decodings
	 
